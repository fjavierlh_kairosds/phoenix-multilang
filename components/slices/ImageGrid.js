import React from 'react';

const ImageGrid = ({ slice }) => {
  const {
    ['image-1']: image1,
    ['image-2']: image2,
    ['image-3']: image3,
  } = slice.primary;
  const images = [image1, image2, image3];
  return (
    <div className="container">
      {images.map((image) => (
        <div key={image.uid} className="col-md-4">
          <img src={image.url} alt={image.alt} />
        </div>
      ))}
    </div>
  );
};

export default ImageGrid;
