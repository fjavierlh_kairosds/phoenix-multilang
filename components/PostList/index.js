import { default as NextLink } from 'next/link';
import { RichText } from 'prismic-reactjs';
import React from 'react';
import { hrefResolver } from '../../prismic-configuration';
import { postListStyles } from '../../styles/components/_postList';

export const PostList = ({ posts = [] }) => {
  return (
    <div className="blog-post-list">
      {posts.map((post) => {
        const { title, ['main-image']: mainImage, content } = post.data;
        const { url, alt } = mainImage;

        return (
          <div key={post.id}>
            <NextLink href={hrefResolver(post)} passHref>
              <h1>{RichText.render(title)}</h1>
            </NextLink>
            <img className="thumbmail" src={url} alt={alt} />
            {RichText.render(content)}
          </div>
        );
      })}
      <style jsx global>
        {postListStyles}
      </style>
    </div>
  );
};
