import Prismic from '@prismicio/client';
import { RichText } from 'prismic-reactjs';
import useUpdatePreviewRef from 'utils/hooks/useUpdatePreviewRef';
import useUpdateToolbarDocs from 'utils/hooks/useUpdateToolbarDocs';
import { Layout } from '../../components';
import { PostList } from '../../components/PostList';
import { Client, manageLocal } from '../../utils/prismicHelpers';
import { homepageToolbarDocs } from '../../utils/prismicToolbarQueries';

const Blog = ({ doc, posts, menu, lang, preview }) => {
  if (doc && doc.data) {
    useUpdatePreviewRef(preview, doc.id);
    useUpdateToolbarDocs(homepageToolbarDocs(preview.activeRef, doc.lang), [
      doc,
    ]);
    return (
      <Layout
        altLangs={doc.alternate_languages}
        lang={lang}
        menu={menu}
        isPreview={preview.isActive}
      >
        {RichText.render(doc.data.title)}
        {RichText.render(doc.data.description)}

        <PostList posts={posts} />
      </Layout>
    );
  }
};

export async function getStaticProps({
  preview,
  previewData,
  locale,
  locales,
}) {
  const ref = previewData ? previewData.ref : null;
  const isPreview = preview || false;
  const refOption = ref ? { ref, lang: locale } : { lang: locale };
  const client = Client();

  const postQueryOptions = { orderings: '[my.post.date desc]', ...refOption };
  const posts = await client.query(
    Prismic.Predicates.at('document.type', 'post'),
    postQueryOptions
  );

  const doc =
    (await client.getSingle(
      'blog',
      ref ? { ref, lang: locale } : { lang: locale }
    )) || {};
    
  const menu =
    (await client.getSingle(
      'top_menu',
      ref ? { ref, lang: locale } : { lang: locale }
    )) || {};

  const { currentLang, isMyMainLanguage } = manageLocal(locales, locale);

  return {
    props: {
      doc,
      menu,
      posts: posts ? posts.results : [],
      preview: {
        isActive: isPreview,
        activeRef: ref,
      },
      lang: {
        currentLang,
        isMyMainLanguage,
      },
    },
  };
}

export default Blog;
