import { Layout, SliceZone } from 'components';
import { RichText } from 'prismic-reactjs';
import React from 'react';
import useUpdatePreviewRef from 'utils/hooks/useUpdatePreviewRef';
import useUpdateToolbarDocs from 'utils/hooks/useUpdateToolbarDocs';
import { Client, manageLocal } from 'utils/prismicHelpers';
import { pageToolbarDocs } from 'utils/prismicToolbarQueries';
import { queryRepeatableDocuments } from 'utils/queries';

/**
 * posts component
 */
const Post = ({ doc, menu, lang, preview }) => {
  if (doc && doc.data) {
    useUpdatePreviewRef(preview, doc.id);
    useUpdateToolbarDocs(
      pageToolbarDocs(doc.uid, preview.activeRef, doc.lang),
      [doc]
    );

    const { title, ['main-image']: mainImage, content } = doc.data;
    const { url, alt } = mainImage;

    return (
      <Layout
        altLangs={doc.alternate_languages}
        lang={lang}
        menu={menu}
        isPreview={preview.isActive}
      >
        {RichText.render(title)}
        {RichText.render(content)}
        <img src={url} alt={alt} />

        {doc.data.body && <SliceZone sliceZone={doc.data.body} />}
      </Layout>
    );
  }
};

export async function getStaticProps({
  preview,
  previewData,
  params,
  locale,
  locales,
}) {
  console.log(`params`, params);

  const ref = previewData ? previewData.ref : null;
  const isPreview = preview || false;
  const client = Client();
  const doc =
    (await client.getByUID(
      'post',
      params.uid,
      ref ? { ref, lang: locale } : { lang: locale }
    )) || {};
  console.log(`doc`, doc);
  const menu =
    (await client.getSingle(
      'top_menu',
      ref ? { ref, lang: locale } : { lang: locale }
    )) || {};

  const { currentLang, isMyMainLanguage } = manageLocal(locales, locale);

  return {
    props: {
      menu,
      doc,
      preview: {
        isActive: isPreview,
        activeRef: ref,
      },
      lang: {
        currentLang,
        isMyMainLanguage,
      },
    },
  };
}

export async function getStaticPaths() {
  const documents = await queryRepeatableDocuments(
    (doc) => doc.type === 'post'
  );
  return {
    paths: documents.map((doc) => {
      return { params: { uid: doc.uid }, locale: doc.lang };
    }),
    fallback: false,
  };
}

export default Post;
